var mysql_promise = require('promise-mysql'),
    connData = require('./connectiondata');

var conn_promise = function(){


    var connection = mysql_promise.createConnection({
        host: connData.host,
        user: connData.user,
        password: connData.password,
        database : connData.db_name 
    })

    return connection;
}
module.exports = conn_promise;
