var mysql = require('mysql'),
    connData = require('./connectiondata');

var connectionPool = function(){


    var pool  = mysql.createPool({
        connectionLimit: connData.connectionLimit,
        host: connData.host,
        user: connData.user,
        password: connData.password,
        database : connData.db_name //mysql database name
    });

    return pool;
}

module.exports = connectionPool;