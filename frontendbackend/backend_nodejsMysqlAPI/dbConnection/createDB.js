var mysql = require('promise-mysql'),
    fs = require('fs'),
    path = require('path');
   

// create db tables
var createTables = function (){
    //get connection to mysql
    var conn = require('./connection')();
    //get sql fies to create tables
    const parentDirs = path.resolve(__dirname);
    const tablesDir = "/tables/";
    const d = parentDirs + tablesDir; 
    
    //one-line function for retrieving sql files from dir 'd'
    const walkSync = (d) => 
                fs.statSync(d).isDirectory() ?  
                fs.readdirSync(d).map( 
                    f => walkSync( path.join(d, f)) 
                ) : d;
    //array of sql files
    let filearr = walkSync(d);
    if(filearr.length){
        for (let i = 0; i < filearr.length; i++) { 
            let sql_f = filearr[i] ;
            let sql = fs.readFileSync(sql_f).toString();
            conn.query(sql);
            console.log("Created table, if it not existed, counted as " + i + ".");
        }
        if(conn.end()){
            console.log("Database connection closed!");
        }
    }
}

//var connections will be used in another files
var createDB = {
    //create mysql database and tables into it
    createDataBase_and_Tables  : function (){
        //get data for mysql connection
        var connData = require('./connectiondata');

        mysql.createConnection({
            host: connData.host,
            user: connData.user,
            password: connData.password
        }).then(function(conn){
            var result = conn.query("CREATE DATABASE IF NOT EXISTS " + connData.db_name);
            conn.end();
            console.log("Successfully created database " + connData.db_name + "!");
            return result;
        }).then(function(){
            // create db tables
            createTables();
        }).catch(function(error){
            console.log("--ERROR- while creating database-", error);
        });
    } 

}

module.exports = createDB;