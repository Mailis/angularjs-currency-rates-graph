var mysql = require('mysql'),
    connData = require('./connectiondata');

var conn = function(){


    var connection = mysql.createConnection({
    host: connData.host,
    user: connData.user,
    password: connData.password,
    database : connData.db_name //mysql database name
    });

    connection.connect(function(err) {
        if (err) throw err;
    });

    return connection;
}



module.exports = conn;