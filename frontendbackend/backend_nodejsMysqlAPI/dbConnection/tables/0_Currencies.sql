CREATE TABLE IF NOT EXISTS currency (
    id INT NOT NULL AUTO_INCREMENT, 
    base VARCHAR(3) NOT NULL, 
    target VARCHAR(3) NOT NULL, 
    rate_date VARCHAR(10) NOT NULL, 
    rate REAL	NOT NULL,
    PRIMARY KEY (id)
    )ENGINE=InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci