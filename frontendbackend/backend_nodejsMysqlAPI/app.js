var express = require('express'),
    bodyParser = require('body-parser'),
    path = require('path'),
    createDB = require('./dbConnection/createDB.js'),
    cors = require('cors');


   
//create db and tables if these don't exist yet
createDB.createDataBase_and_Tables();


var app = express();
app.use(cors());
var port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



/*routers here*/

var currencyRoutes = require('./routes/currencyRoutes'); //importing route
var router = express.Router();
app.use('/api/curr', router);
currencyRoutes(router);

app.get('/api', function (req, res){
    res.send('Welcome to GULP powered API!')
});

//redirect in case of an error
app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
  });

app.listen(port, function(){
    console.log("GULP powered my API on a server listening to port " + port);
});
