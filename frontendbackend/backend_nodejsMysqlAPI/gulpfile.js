var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    gulpMocha = require('gulp-mocha'),
    env = require('gulp-env'),
    supertest = require('supertest');


gulp.task('default', function (){
    nodemon({
        scrit: 'app.js',
        ext: 'js',
        env: {
            PORT: 8000
        },
        ignore: ['./node_models']
    })
    .on('restart', function(){
        console.log('Restarting');
    })
});

//testing
gulp.task('test', function(){
    env({vars: {ENV: 'Test'}});
    gulp.src('tests/*.js', {read: false})
    .pipe(gulpMocha({reporter: 'nyan'}));
});