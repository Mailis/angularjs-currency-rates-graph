var express = require('express');



module.exports = function(router) {
    var currencyController = require('../controllers/currencyController');

    
    router
        .get('/', currencyController.list_all_currencies)
        .post('/', currencyController.post_a_currency)
        .get('/:base/:target/:start/:end', currencyController.get_currencies_by_dates); 

       // .get('/:date/:base/:target', currencyController.list_currencies_by_params) 


};

