var currencyModel = require('../models/currencyModel'),
    connectionPool = require('../dbConnection/connectionPool'),
    connData = require('../dbConnection/connectiondata');

var pool = connectionPool();

exports.list_all_currencies = function(req, res) {
        pool.getConnection(function(err, conn) {
            conn.query('SELECT * FROM currency', function (error, results, fields) {
                if(error){
                    res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
                    //If there is error, we send the error in the error section with 500 status
                } else {
                    res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
                    //if msql does not have data
                    if(results === []){
                        //ask data from fixer.io
                    }
                }
                conn.release();
            });

        });
    
};


//GET

/**
 * get route by params '/:base/:target/:start/:end'
 * @param {*} req request
 * @param {*} res response
 */
exports.get_currencies_by_dates = function(req, res) {
    // console.log("REQ params", req.params);
    // console.log("type REQ params", typeof(req.params));
    var q = filterRequestParams(req.params);
    var queryResult = {};
    // console.log("q", q);
    // console.log("type q", typeof(q));

    if(q){
        // var fixerioUrl = "https://api.fixer.io/" + query.date + "?base=" + query.base + "&symbols=" + query.target;
        processData_fromDB(q, res);
    }
}


/**
 * Callback from promise for handling error
 * @param {*} err error object
 */
var errHandler = function(err) {
    console.log(err);
}

/**
 * Returns array of currency Objects, e.g. {"base":"EUR","target":"GBP","rate_date":"2016-01-12","rate":"1.2"}
 * @param {*} queryResultObj raw response data from db SEECT
 */
function genCurrenciesListFromDbSelectResult(queryResultObj){
    var currencyModelsArray = []
    for(var i in queryResultObj){ 
        var currModel = {};
        Object.entries(queryResultObj[i]).map(([key,value]) => {
            currModel[key] = value;
        });      
        currencyModelsArray.push(currModel);    
    } 
    return currencyModelsArray;
}

/**
 * Validates, if all params exists
 * @param {*} params request params
 */
function filterRequestParams(params){
    var query = {};
    //avoid random user input
    if(params.base){
        query.base = params.base;
        if(params.target){
            query.target = params.target;
            if(params.start){
                query.start = params.start;
                if(params.end){
                    query.end = params.end;
                    return query;
                }
            }
        }
    }
    return null;
}

//get by params'/:date/:base/:target'
/**
 * using params, makes fixer.io API URL
 * sends request params to fixer.io API
 * returns resulting JSON object (on success) or null (on error)
 * @param {*} params request params
 */
function getFixerioPromise(query) {
        var fixerioUrl = "https://api.fixer.io/" + query.date + "?base=" + query.base + "&symbols=" + query.target;
        //console.log('fixerioUrl', fixerioUrl);
        var request = require('request');
        var options = {
            url: fixerioUrl,
            headers: {
                'User-Agent': 'request'
            }
        };
        return new Promise(function(resolve, reject) {
            // Do async job
            request.get(options, function(err, resp, body) {
                if (err) {
                    reject(err);
                } else {
                    //console.log('fixerio body', body);
                    resolve(body);
                }
            })
        })
};

/**
 * Cleans data from fixerio query into useful structure
 * @param {*} fixerioResult JSON string, req result from fixerio API
 * input (fixerioResult)
 *  structure is
 *         {
                "base":"EUR",
                "date":"2016-01-18",
                "rates":{
                    "GBP":0.76263
                }
            }
 * results in a structure
 *          { 
 *          base: 'EUR',
            target: 'GBP',
            rates:
            [ { '2013-01-11': 0.8232 },
                { '2013-01-14': 0.8312 },
                { '2013-01-21': 0.839 },
                { '2013-01-28': 0.8545 },
                { '2013-02-18': 0.8619 },
                { '2013-02-04': 0.8619 },
                { '2013-02-20': 0.8733 },
                { '2013-02-11': 0.8535 } ] 
            }
 */
function addFixerioDataToArray(fixerioResult, resArray){
    //console.log("fixerioObj PROMISE", result);
    /*
    fixerioObj result is e.g. 
    {
        "base":"EUR",
        "date":"2016-01-18",
        "rates":{
            "GBP":0.76263
        }
    }
    */
    let resultObj = JSON.parse(fixerioResult);
    let rateValue = (Object.values(resultObj.rates))[0];
    let fixerRateData = {};
    fixerRateData[resultObj.date] = rateValue;
    resArray.rates.push(fixerRateData);
    return resArray;
}

/**
 * Compares wether dbData has all q data, if not, asks it from fixer.io
 * @param {*} dbData array of currency ojects, retrieved from mysql database
 * @param {*} q Object of the requested data (base/target/start/end) e.g. /EUR/GBP/2016-01-12/2016-01-12
 */
function compareRequestAndMysqlDates(dbData, q, res){
    if(!q) return null;

    let requestedDatesArray = generateRequestDates(2, q.start, q.end);
    let mysqlDatesArray = generateMysqlDates(dbData);
    //store missed dates in separate array
    let missedDates = [];
    // console.log("Q", q);
    // console.log("requestedDatesArray", requestedDatesArray);
    // console.log("mysqlDatesArray", mysqlDatesArray);
    let mergedObjects = {};
    mergedObjects["base"] = q.base;
    mergedObjects["target"] = q.target;
    mergedObjects["rates"] = [];
    //if array from db lacks of some data, ask it from fixer.io
    for(let i in requestedDatesArray){
        let reqDate = requestedDatesArray[i];
        // console.log("reqDate", reqDate);
        if(mysqlDatesArray.indexOf(reqDate) < 0){ //db result does not contain requested date
            missedDates.push(reqDate);
        }
        else{
            //get not-missing data from dbData
            for(let j in dbData){
                let o = dbData[j];
                if(o.rate_date === reqDate){
                    let dbData_tmp = {};
                    dbData_tmp[reqDate] = o.rate;
                    //console.log("dbData PROMISE  o in dbData", o);
                    mergedObjects["rates"].push(dbData_tmp);
                }
            }
        }
    }
    if(missedDates.length > 0){
        //console.log('MISSED DATES', missedDates);
        var fixerioDataRates = mergeFixerIoDataArrayAndInsertToDb(q, missedDates, mergedObjects, res);
    }   
    else{
        //all the data is present, send result
        res.send(mergedObjects);
    }
}

/**
 * Merges db and fixerio data, then inserts merged result to db
 * 
 * @param {*} res response object, for sending response back to a client
 * @param {*} q JSON insertion object
 * @param {*} missedDates array of dates, that was not present in db
 * @param {*} mergedObjects JSON object with records of rates from db
 */
function mergeFixerIoDataArrayAndInsertToDb(q, missedDates, mergedObjects, res){
    let promisesArray = [];

    // add missed dates from fixer.io inside separate loop 
    if(missedDates.length > 0){
        for(let i in missedDates){
            let missingDate = missedDates[i];
            //prepare object for fixerio api
            var qTmp = {};
            qTmp["base"] = q.base;
            qTmp["target"] = q.target;
            qTmp["date"] = missingDate;
            //get  requested data from fixerio
            var fixerioObj = getFixerioPromise(qTmp);
            promisesArray.push(fixerioObj);
        }
    }
    
    

    Promise.all(promisesArray).then(function(values) {
        for (let i in values){
            //console.log('promisesArray VALUE[i]', values[i]);
            addFixerioDataToArray(values[i], mergedObjects);
        }
        //console.log('mergedObjects', mergedObjects);
        // console.log('+++++++++++++++++++++++++');
        //sort rates by dates in asc order 
        let rates_sort = mergedObjects.rates;
        sortObject(rates_sort);
        mergedObjects["rates"] = rates_sort;
        // send response to client- mergedObjects;
        res.send(mergedObjects);

        //insert missing data to db
        for(let i in mergedObjects.rates){            
            let postData = {};
            postData.base = mergedObjects.base;
            postData.target = mergedObjects.target;
            postData.rate_date = (Object.keys(mergedObjects.rates[i]))[0];
            postData.rate = (Object.values(mergedObjects.rates[i]))[0];
            insertCurrencyToMysql(postData);
        }

    });
}

/**
 * Returns sorted by keys (dates) jsonObject
 * @param {*} o Unsorted jsonObject, 
 *              with format { "date": rate }
 *                     e.g. { "2018-02-15": 0.76448, "2018-01-22": 0.76954}
 */
function sortObject(o) {
    return Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});
}

/**
 * Returns array of  date strings from array of dbData objects
 * @param {*} dbData array of currency ojects, retrieved from mysql database
 */
function generateMysqlDates(dbData){
    var mysqlDatesArray = []
    for(var i in dbData){ 
        //console.log("mysqlDatesArray i ", dbData[i]);
        let dObj = dbData[i].rate_date;
        //avoid duplicates
        if(mysqlDatesArray.indexOf(dObj) < 0){
            mysqlDatesArray.push(dObj);  
        }     
    } 
    return mysqlDatesArray.sort();
}

/**
 * 
 * @param {*} dateObj Date() object 
 * returns formatted ( 'yyyy-mm-dd' )  string of date 
 */
function formateDate(dateObj){
    const day = addZeroPad( dateObj.getDate() );
    const month = addZeroPad( dateObj.getMonth() + 1 );
    const year = dateObj.getFullYear();
    //according to api, a query date must be
    //formatted as '2000-01-03' year-month-day
    //  https://api.fixer.io/2000-01-03
    return year + "-" + month + "-" + day;
}


/**
 * adds zero to the front of a number that are smaller than 10
 * @param {*} nmbr number
 */
function addZeroPad(nmbr){
    return ( (nmbr < 10) ? ("0" + nmbr) : nmbr );
}

/**
 * Returns array of formatted ( 'yyyy-mm-dd' )  string of date 
 * @param {*} weekDayNr nr of the day in a week, e.g. nr 1 represents Sunday
 * @param {*} startDate string object of date ( 'yyyy-mm-dd' ) to start with
 * @param {*} endDate string object of date ( 'yyyy-mm-dd' ) to end with
 */
function generateRequestDates(weekDayNr, _startDate, _endDate){
    let datesArray = [];
    let startEqualsEnd = (_startDate ===_endDate)? true : false;
    
    var startDate = new Date(_startDate);
    //add start date to the array
    datesArray.push(formateDate(startDate));

    //if startdate and enddate are equal, go no further
    if(!startEqualsEnd){
        var endDate = new Date(_endDate);
        //number to add:
        //it is 1 until it meets a nuber of 'weekDayNr'
        //then it becomes 7
        let weekCounter = 1;
        for(let d = startDate; d < endDate; ){
            d = new Date(startDate.setDate(startDate.getDate() + weekCounter));
            //detect day of a week
            if( (d.getDay() + 1) === weekDayNr){
                //if computed weekday is smaller than that end date
                if( d < endDate){
                    datesArray.push(formateDate(d));
                    weekCounter = 7;
                }
            }
        }
        //add end date to the array
        //returns formatted ( 'yyyy-mm-dd' )  string of date 
        datesArray.push(formateDate(endDate));
    }
    //console.log('datesarray', datesArray);
    return datesArray;
}

/**
 * Promise pipes, query to db ->  processing q results
 * @param {*} q Object of requast data
 */
function processData_fromDB(q, res){
    var queryresult = {};
    var dataPromise = getCurrenciesByStartEndBaseTarget_fromDB(q);
    dataPromise.then(function(result) {
                    return genCurrenciesListFromDbSelectResult(result);
                }, errHandler)
                .then(function(dbData) {
                    compareRequestAndMysqlDates(dbData, q, res);
                }, errHandler);
}

/**
 * makes SELECT query against database
 * returns db query result
 * @param {*} q Object of the requested data (base/target/start/end) e.g. /EUR/GBP/2016-01-12/2016-01-12
 */
function getCurrenciesByStartEndBaseTarget_fromDB(q){
    return new Promise(function(resolve, reject) {
        pool.getConnection(function(err, conn) {
            conn.query('SELECT * FROM currency WHERE rate_date >= ? AND rate_date <= ? AND base = ? AND target = ?',[q.start, q.end, q.base, q.target],  
                function (error, results, fields) {
                    conn.release();
                    //console.log('_fromDB RESULTS', results);
                    if(!error){
                        resolve(results);
                    }
                    else{
                        reject(error);
                    }
            });
        });
    })
}



//POST
//post example: {"base":"EUR","target":"GBP","rate_date":"2016-01-05","rate":"33"}
exports.post_a_currency = function(req, res) {
    // console.log("REQ BODY", req.body);
    // console.log("type REQ BODY", typeof(req.body));
    postCurrencyToMySql(req, res);
};



function postCurrencyToMySql(req, res){
    var error = null;
    if(!req.body.base){
        res.status(400);
        error = ('Base currency is required!');
    }
    else if(!req.body.target){
        res.status(400);
        error = ('Target currency is required!');
    }
    else if(!req.body.rate_date){
        res.status(400);
        error = ('Date is required!');
    }
    else if(!req.body.rate){
        res.status(400);
        error = ('Rate is required!');
    }
    else{
        var postData  = req.body;
        //check if the same data exists
        let insertResult = insertCurrencyToMysql(postData);
        res.send(insertResult);
    }

    if(error){
        res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
    }
}

/**
 * 
 * @param {*} q json object to insert into mysql:
 *              q.base, q.target, q.rate_date, q.rate]
 * 
 * B it checks if the same record exists already, in order to avoid duplicates
 */
function checkIfRecorddExists(q){
    //'SELECT * FROM currency WHERE base = "EUR" AND target = "SEK" AND rate_date = "2015-12-31"' 
    //[q.base, q.target, q.rate_date, q.rate]
    var promise1 = new Promise((resolve, reject) => {
        let isPresentInDb = false;

        pool.getConnection(function(err, conn) {
            conn.query('SELECT * FROM currency WHERE base = ? AND target = ? AND rate_date = ? AND rate = ?', [q.base, q.target, q.rate_date, q.rate],
                function (err, result) {
                    conn.release();
                    if(result.length > 0){
                        isPresentInDb = true;
                    }
                    // console.log('PROMISE 1 result', result);
                    // console.log('PROMISE 1', isPresentInDb);
                    resolve(isPresentInDb);
                }
            );
        });
    });
    return promise1;
}

/**
 * 
 * @param {*} q json object to insert into mysql:
 *              q.base, q.target, q.rate_date, q.rate]
 * 
 * Before inserting, it checks if the same record exists already, in order to avoid duplicates
 */
function insertCurrencyToMysql(q){

    //checks if the same record exists already, in order to avoid duplicates
    var promise1 = checkIfRecorddExists(q);
    //save to db
    promise1
        .then(function(isPresentInDb) {
            //console.log('isPresentInDb', isPresentInDb);
            if(!isPresentInDb){
                pool.getConnection(function(err, conn) {
                    conn.query('INSERT INTO currency SET ?', q,
                        function (err, result) {
                            conn.release();
                            if (err) return err; //throw err;
                            return result;
                        }
                    );
                });

            }
        }, errHandler);
    
}