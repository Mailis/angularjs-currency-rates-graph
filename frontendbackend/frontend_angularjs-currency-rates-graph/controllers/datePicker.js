
app.controller('datePicker', function($scope,  $rootScope, InputCollectionService) {
    //check previous user selection
    let saved_sd = new Date(angular.fromJson(window.localStorage['sDate']));
    let saved_ed = new Date(angular.fromJson(window.localStorage['eDate']));
    
    //31 days back from today
    let monthAgoDate =  new Date(new Date().setDate(new Date().getDate()-31));
    $scope.startDate = (saved_sd === "null")? monthAgoDate : saved_sd;
    //today
    $scope.endDate = (saved_ed === "null")? new Date() : saved_ed;
    //console.log('$scope.startDate, $scope.endDate', $scope.startDate, $scope.endDate);
    
    //prefill inputCollection
    InputCollectionService.addDates($scope.startDate, $scope.endDate);

    //validation of user-selected dates
    $scope.showMe = validateDateInterval($scope.startDate, $scope.endDate);

    var vm = this;
    $scope.onDateChanged = function() {
        //console.log('onDateChanged ONDATECHANGED called');
        //console.log('stat d end d',$scope.startDate, $scope.endDate);
        //dont display both errors
        let isWrongOrder = validateSecondIsLater($scope.startDate, $scope.endDate);
        let isWrongInterval = validateDateInterval($scope.startDate, $scope.endDate);
        $scope.moreThanMonth = isWrongOrder? false : isWrongInterval;
        $scope.firstIsLast = isWrongOrder;

        //add user-selected dates into InputCollection
        if(!(isWrongOrder || isWrongInterval)){
            InputCollectionService.addDates($scope.startDate, $scope.endDate);
            vm.data = angular.copy(InputCollectionService.getInputObject());
            $rootScope.$broadcast('data_updated', vm.data);
        }
        //InputCollectionService.testData();
    }
    
});