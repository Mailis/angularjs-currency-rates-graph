let apiBaseUrl = "http://localhost:8000/api/curr/";

app.controller('drawGraph', function($scope, $http, InputCollectionService) {
    //define AAPI basic URL !
    init();
    //Watch for changes in the service
    $scope.$on('data_updated', 
        function(){
            init();
      }); /* */ 

    function init(){
        //console.log('data_updated', data);
            //validate input integrity
            const isInputDataValid = InputCollectionService.isInputDataCompleted();
            //console.log('isInputDataValid', isInputDataValid);
            if(isInputDataValid){
                //create api query strings
                
                let saved_sd = new Date(angular.fromJson(window.localStorage['sDate']));
                let saved_ed = new Date(angular.fromJson(window.localStorage['eDate']));
               
                let startDate = (saved_sd === undefined)? 
                InputCollectionService.getStartDate() : saved_sd;
                //today
                let endDate = (saved_ed === undefined)? InputCollectionService.getEndDate() : saved_ed;
                
                let baseCurrency = InputCollectionService.getBaseCurrency();
                let targetCurrency = InputCollectionService.getTargetCurrency();
                
                let apirequestUrl = getRequestStrings( baseCurrency, targetCurrency, formateDate(startDate), formateDate(endDate));
                
                //consoleconsole.log('apirequestUrl', apirequestUrl);

                /*
                //TEST the response object
                    let testResponse = '{"base":"EUR","target":"SEK","rates":[{"2010-02-26":9.726},{"2010-03-01":9.7649},{"2010-03-08":9.6794},{"2010-03-15":9.7143},{"2010-03-22":9.7585},{"2010-03-29":9.776},{"2010-04-01":9.7288}]}';
                    
                    let datesRates={};
                    let testResponseObj = JSON.parse(testResponse); 
                    let testRates = testResponseObj.rates;
                    let testRatesteObj = getApiObject(testRates, datesRates);
                    console.log('testRateateObj', testRatesteObj);
                    let sortedDatesRates = sortObject(testRatesteObj);
                    let xAxis = Object.keys(sortedDatesRates);
                    let yAxis = Object.values(sortedDatesRates);
                    $scope.canvas = getCanvasData(xAxis, yAxis, baseCurrency, targetCurrency);
                */
                
                let datesRates={};
                getApiRatesDataFromInterval(
                    apirequestUrl,
                    function(response){
                        let rateseObj = getApiObject(response.data.rates, datesRates);
                        //console.log('rateseObj', rateseObj);
                        let sortedDatesRates = sortObject(rateseObj);
                        let xAxis = Object.keys(sortedDatesRates);
                        let yAxis = Object.values(sortedDatesRates);
                        $scope.canvas = getCanvasData(xAxis, yAxis, baseCurrency, targetCurrency);
                    }
                );
            } 
    }

    /**
     * Returns sorted by keys (dates) jsonObject
     * @param {*} o Unsorted jsonObject, 
     *              with format { "date": rate }
     *                     e.g. { "2018-02-15": 0.76448, "2018-01-22": 0.76954}
     */
    function sortObject(o) {
        return Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});
    }

    
    /**
     * Async calls to fixer.io API
     * callback function inside
     * @param {*} apirequestUrl url string of URLs to fixer.io API 
     * @param {*} callback callback function for handling asynchronousity
     */   
   function  getApiRatesDataFromInterval (apirequestUrl, callback) {
        $http.get(apirequestUrl).
            then(
                function(response) {
                    callback(response);
                }, 
                function(response) {
                    //Second function handles error
                    $scope.content = "Something went wrong";
                    $scope.statuscode = response.status;
                    $scope.statustext = response.statusText; 
                }
            );
    }
});


/**
 * Returns array of formatted ( 'yyyy-mm-dd' )  string of date 
 * @param {*} weekDayNr nr of the day in a week, e.g. nr 1 represents Sunday
 * @param {*} startDate Date() object to start with
 * @param {*} endDate Date() object to end with
 */
function getDatesOnWeekday(weekDayNr, startDate, endDate){
    let datesArray = [];

    //add start date to the array
    datesArray.push(formateDate(startDate));
    //number to add:
    //it is 1 until it meets a nuber of 'weekDayNr'
    //then it becomes 7
    let weekCounter = 1;
    for(let d = startDate; d < endDate; ){
        d = new Date(startDate.setDate(startDate.getDate() + weekCounter));
        //detect day of a week
        if( (d.getDay() + 1) === weekDayNr){
            //if computed weekday is smaller than that end date
            if( d < endDate){
                datesArray.push(formateDate(d));
                weekCounter = 7;
            }
        }
    }
    //add end date to the array
    //returns formatted ( 'yyyy-mm-dd' )  string of date 
    datesArray.push(formateDate(endDate));
    return datesArray;
}

/**
 * extracts date and respective rate data from fixer.io API query response
 * Returns jsonObject in format { "date" : rate}
 * @param {*} responseData json response from fixer.io API
 * @param {*} dateRate jsonObject where to save extracted data
 */
function getApiObject(responseData, dateRate){
    Object.entries(responseData).map(([key,value]) => {
        Object.entries(value).map(([key2,value2]) => {
            dateRate[key2] = value2;
        });
    });
    return dateRate;
}

/**
 * Returns an array of requests URL strings, 
 * which will be used for making queries against fixer.io API
 * @param {*} datesArray array of dates ( Date() ) ( start, ..., end )
 * @param {*} baseC baseCurrency
 * @param {*} targetC targetCurrency
 * @param {*} apiBaseUrl basic url to API
 */
//function getRequestStrings(datesArray, baseC, targetC, sDate, eDate, _apiBaseUrl = apiBaseUrl){
function getRequestStrings(baseC, targetC, sDate, eDate, _apiBaseUrl = apiBaseUrl){
    return  apiBaseUrl + baseC + "/" + targetC + "/" + sDate + "/" + eDate;
}

/**
 * Required data for drawing canvas
 * @param {*} datesArray dates
 * @param {*} ratesArray rate respective to date
 * @param {*} baseCurrency base Currency
 * @param {*} targetCurrency target currency
 */
function getCanvasData(datesArray, ratesArray, baseCurrency, targetCurrency){
    //console.log('CANVAS');
    var ctx = document.getElementById("myLineChart");
    var currencyChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: datesArray,
                        datasets: [{
                            label: 'base: ' + baseCurrency + " target: " + targetCurrency,
                            data: ratesArray,
                
                            backgroundColor: ['rgba(3, 245, 64, 0.521)'],
                            lineTension : 0,
                            borderColor : 'orangered',
                            pointRadius: 7,
                            pointBorderWidth: 3,
                            pointHoverRadius: 9,
                            pointHoverBackgroundColor: 'rgba(3, 245, 64, 0.521)'
                        }],
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:false
                                }
                            }]
                        }
                    }
                }
            );
    return ctx;
}