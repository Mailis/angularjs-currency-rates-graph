
//collected input data service
app.service('InputCollectionService', function(){
    var collectedInputData = {};
    
    this.addCurrency = function (baseC, targetC){
        collectedInputData.baseCurrency = baseC;
        collectedInputData.targetCurrency = targetC;
        window.localStorage['base'] = JSON.stringify(baseC);
        window.localStorage['target'] = JSON.stringify(targetC);
        return collectedInputData;
    };

    this.addDates = function(sDate, eDate){
        console.log('typeof sDate', typeof(sDate));
        console.log('typeof eDate', typeof(eDate));
        //validation
        let minFixerioDate = new Date('1999-01-01');
        console.log('TEST minFixeruiDate DATE', minFixerioDate);
        if(typeof(sDate) == typeof(new Date())){
            if(sDate < minFixerioDate){
                sDate = minFixerioDate;
            }
        }
        else{
            sDate =  new Date(new Date().setDate(new Date().getDate()-31));
        }
        if(typeof(eDate) == typeof(new Date())){
            if(eDate < minFixerioDate){
                eDate = minFixerioDate;
            }
        }
        else{
            eDate = new Date();
        }
        // console.log('sDate, eDate', sDate, eDate);
        window.localStorage['sDate'] = JSON.stringify(sDate);
        window.localStorage['eDate'] = JSON.stringify(eDate);
        
        collectedInputData.startDate = sDate;
        collectedInputData.endDate = eDate;
        return collectedInputData;
    };

    this.getInputObject = function(){
        return collectedInputData;
    };

    this.isInputDataCompleted = function(){
        return allDataIsPresent(collectedInputData);
    };


    this.getStartDate = function(){
        return collectedInputData.startDate;
    };

    this.getEndDate = function(){
        return collectedInputData.endDate;
    };
    this.getFormattedStartDate = function(){
        return formateDate(collectedInputData.startDate);
    };

    this.getFormattedEndDate = function(){
        return formateDate(collectedInputData.endDate);
    };


    this.getBaseCurrency = function(){
        return collectedInputData.baseCurrency;
    };

    this.getTargetCurrency = function(){
        return collectedInputData.targetCurrency;
    };

    this.testData = function(){
        console.log('collectedInputData', collectedInputData);
    };
});

/**
 * 
 * @param {*} dateObj Date() object 
 * returns formatted ( 'yyyy-mm-dd' )  string of date 
 */
function formateDate(dateObj){
    const day = addZeroPad( dateObj.getDate() );
    const month = addZeroPad( dateObj.getMonth() + 1 );
    const year = dateObj.getFullYear();
    //according to api, a query date must be
    //formatted as '2000-01-03' year-month-day
    //  https://api.fixer.io/2000-01-03
    return year + "-" + month + "-" + day;
}

/**
 * adds zero to the front of a number that are smaller than 10
 * @param {*} nmbr number
 */
function addZeroPad(nmbr){
    return ( (nmbr < 10) ? ("0" + nmbr) : nmbr );
}

//validation
/**
 * Checks if object exists
 * @param {*} obj  jsn object
 */
function isNot_Empty_Null_Undefined(obj){
    let isValidObj = true;
    if(obj === null || obj === undefined || obj === "" ){
        isValidObj = false;
    }
    return isValidObj;
}

/**
 * Checks if all necessary data is inserted by user
 * @param {*} jsonObj 
 */
function allDataIsPresent(jsonObj){
    let isValidObj = true;
    Object.values(jsonObj).map((x) => {
        if(!isNot_Empty_Null_Undefined(x)){//input value is not valid
            isValidObj = false;
        }
    });
    return isValidObj;
}