var app = angular.module('currenciesApp', [])

app.controller('currencySelector', function($scope,  $rootScope, $http, InputCollectionService) {
    var vm = this;

    //user changed currency:
    $scope.onSelectCurrencyChanged = function() {
        //when both currencies are the same,
        //inValidSelect = true
        let inValidSelect = validateCurrencySelection($scope.baseCurrency, $scope.targetCurrency);
        $scope.showSelectionError = inValidSelect;
        //insert into 
        //if both currencies are selected
        let areBothInserted = hasBothCurrencies($scope.baseCurrency, $scope.targetCurrency);
        if(areBothInserted && !inValidSelect){
            InputCollectionService.addCurrency($scope.baseCurrency, $scope.targetCurrency);
            vm.data = angular.copy(InputCollectionService.getInputObject());
            $rootScope.$broadcast('data_updated', vm.data);
        }
        //InputCollectionService.testData();
    }
    
    var getStoredCurrencies = function(){
        let baseC = angular.fromJson(window.localStorage['base']);
        let targetC = angular.fromJson(window.localStorage['target']);
        //store it if currency data are available
        let smthChanged = false;

        if(baseC){
            $scope.baseCurrency = baseC;
            smthChanged = true;
        }
        if(targetC){
            $scope.targetCurrency = targetC;
            smthChanged = true;
        }
        if(smthChanged){
            InputCollectionService.addCurrency($scope.baseCurrency, $scope.targetCurrency);
        }
    } 
    getStoredCurrencies();
    /* */
    //fill currencies only at start
    var init = function () {
        //console.log('http called');
        $http.get('https://api.fixer.io/latest').
            then(function(response) {
                //First function handles success
                //base is EUR
                //add eur to response.data
                //sort array
                $scope.fixerCurrency = listCurrencies(response.data);
            }, 
            function(response) {
                //Second function handles error
                $scope.content = "Something went wrong";
                $scope.statuscode = response.status;
                $scope.statustext = response.statusText; 
            });
    }
    init();
});

/**
 * check if user has already selected currencies, so
 * on window reload can prefill this selection
 */
function getStoredCurrencies(){
    
}

function listCurrencies(dataObj){
    let currenciesArr = [dataObj.base];
    Object.entries(dataObj.rates).map(([key, value]) => {
        currenciesArr.push(key);
    });
    currenciesArr.sort();
    return currenciesArr;
}