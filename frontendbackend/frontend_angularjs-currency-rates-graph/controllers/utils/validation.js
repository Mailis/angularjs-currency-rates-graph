

//user input validation

function validateDateInterval(startDate, endDate){
    console.log('validate startDate',startDate);
    console.log('validate endDate', endDate);
    var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
    console.log('validate diffDays', diffDays);
    return (diffDays < 31);
}

function validateSecondIsLater(startDate, endDate){
    return (endDate < startDate);
}

function hasBothCurrencies(curr1, curr2){
    let areBothPresent = true;
    if(!curr1 || !curr2) {
        areBothPresent = false;
    }
    if(curr1 === undefined || curr2 === undefined) {
        areBothPresent = false;
    }
    return areBothPresent;
}

function validateCurrencySelection(curr1, curr2){
    return (curr1 === curr2);
}

