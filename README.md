
---------------------------------------------------------------------------------

to start backend server, go to 
directory

$ cd frontendbackend/backend_nodejsMysqlAPI/

install packages:

$ npm install

start server:

$ gulp

---------------------------------------------------------------------------------

About:

---


FRONTEND:

Web site using Angular JS framework version (1.x). 
The purpose of the website is to show illustrative graphic of the currency rates graphically. 
 
The website has two different user inputs:


First group of the inputs are: 

Start date

End Date
 
 

Second group of the inputs are:

Base Currency

Target Currency

 
Minimum date difference user can select is 1 month.

Once the user has chosen the currency rates and the dates, the website shows the currency rates data from the http://fixer.io/ website and displays a graphic how the rate has changed. Shows rates weekly, on every Monday. 

---------------------------------------------------------------------------------

BACKEND:

Angular JS  fetches the data from nodeJS web server as an extra layer that fetches the data from fixer.io
Backend stores the data also in MySQL database - in case the data exists for the given currency rate and a date, no need to get it from the fixer.io web site.


---------------------------------------------------------------------------------

